import { Component } from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {WeekDaysService} from '../week-days.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  checkData: any;
  dataPush: any;
  group = 0;
  max: any;
  constructor(private route: ActivatedRoute, private router: Router, private weekService: WeekDaysService) {
      this.checkData = this.weekService.acceptData;
      console.log(JSON.stringify(this.dataPush));
  }
    checkDataView() {
    }
  next() {
       this.group++
       this.checkData.map(dataGroup => {
          if (dataGroup.isChecked && !dataGroup.isConfig) {
              dataGroup.group = this.group;
              if (dataGroup.group <= this.group) {
                  this.max = dataGroup.weekDay;
                  dataGroup.max = this.max;
                  console.log('dataGroupMax', dataGroup.weekDay);
                  console.log('max', dataGroup.max);
              }
          }
      });
       this.weekService.acceptData = this.checkData;
       this.router.navigate(['next-page']);
  }
}
