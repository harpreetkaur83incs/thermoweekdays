import { Injectable } from '@angular/core';
// const acceptData =  [
//   {isChecked: false, userDay: 'Monday', weekDay: 'Mon', group: 0, isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Tuesday', weekDay: 'Tues', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Wednesday', weekDay: 'Wed', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Thursday', weekDay: 'Thur', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Friday', weekDay: 'Fri', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Saturday', weekDay: 'Sat', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
//   {isChecked: false, userDay: 'Sunday', weekDay: 'Sun', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
// ];
@Injectable({
  providedIn: 'root'
})

export class WeekDaysService {
   acceptData =  [
  {isChecked: false, userDay: 'Monday', weekDay: 'Mon', group: 0, isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Tuesday', weekDay: 'Tues', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Wednesday', weekDay: 'Wed', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Thursday', weekDay: 'Thur', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Friday', weekDay: 'Fri', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Saturday', weekDay: 'Sat', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  {isChecked: false, userDay: 'Sunday', weekDay: 'Sun', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
];
  updateValue =  [
    {isChecked: false, userDay: 'Monday', weekDay: 'Mon', group: 0, isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Tuesday', weekDay: 'Tues', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Wednesday', weekDay: 'Wed', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Thursday', weekDay: 'Thur', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Friday', weekDay: 'Fri', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Saturday', weekDay: 'Sat', isConfig: false, getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
    {isChecked: false, userDay: 'Sunday', weekDay: 'Sun', isConfig: false,  getData: [{ name: 'Wake', time: '06:00', temperature: 82}, { name: 'Leave', time: '09:00', temperature: 74}, { name: 'Return', time: '05:00', temperature: 82},  { name: 'Sleep', time: '11:00', temperature: 44}]},
  ];
  constructor() { }
}
