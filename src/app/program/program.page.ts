import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {WeekDaysService} from '../week-days.service';

@Component({
  selector: 'app-program',
  templateUrl: './program.page.html',
  styleUrls: ['./program.page.scss'],
})
export class ProgramPage implements OnInit {
  programData2: any;
  constructor(private route: ActivatedRoute, private router: Router, private weekDaysServiceProgram: WeekDaysService) {
    console.log('nextPage ====>' + JSON.stringify(this.weekDaysServiceProgram.acceptData));
    this.programData2 = this.weekDaysServiceProgram.acceptData;
   }

  ngOnInit() {
  }

}
