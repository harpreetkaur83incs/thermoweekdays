import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import { Location } from '@angular/common';
import {WeekDaysService} from '../week-days.service';
@Component({
  selector: 'app-next-page',
  templateUrl: './next-page.page.html',
  styleUrls: ['./next-page.page.scss'],
})
export class NextPagePage implements OnInit {
  dataWeek: any;
  dataUpdate: any;
  constructor(private route: ActivatedRoute, private router: Router, private location: Location, private weekDaysService: WeekDaysService) {
    console.log('nextPage ====>' + JSON.stringify(this.weekDaysService.acceptData));
    this.dataWeek = this.weekDaysService.acceptData;
    this.dataUpdate = this.weekDaysService.updateValue;
  }
  ngOnInit() {
  }
  nextData() {
    let i = 0;
    let k = 0;
    for (i = 0; i < this.dataWeek.length; i++) {
      this.dataUpdate[i].getData = this.dataWeek[i].getData;
    }
    for (k = 0; k < this.dataUpdate.length; k++) {
      console.log('updateData', this.dataUpdate[k].getData);
    }
    let count = 0;
    this.dataWeek.map((checked: any) => {
      if (checked.isChecked === true) {
        count++;
        checked.isConfig = true;
        // console.log('Hello', checked.isConfig);
      }
    });
    this.weekDaysService.acceptData = this.dataWeek;
    this.weekDaysService.updateValue = this.dataUpdate;
    console.log('afterIsConfig', this.weekDaysService.acceptData);
    if (count === this.dataWeek.length) {
      this.router.navigate(['program']);
    } else {
      this.gotoBack();
    }
  }
  gotoBack() {
    this.location.back();
  }
}
