import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextPagePage } from './next-page.page';

describe('NextPagePage', () => {
  let component: NextPagePage;
  let fixture: ComponentFixture<NextPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
